import {observable, computed} from 'mobx';

export default class UserModel {
  @observable uid;
  @observable username;
  @observable avatar;
  @observable email;

  role;

  constructor(user) {
    this.uid = user.uid;
    this.email = user.email;
    this.username = user.username;
    this.avatar = user.avatar;
  }

  @computed
  get shortname() {
    if (!this.username) return null;

    const words = this.username.split(' ');

    if (words.length === 1) {
      const word = words[0];
      return word[0].toUpperCase() + word[1];
    }

    return words
      .filter(s => s.length)
      .map(s => s[0])
      .join('');
  }
}
