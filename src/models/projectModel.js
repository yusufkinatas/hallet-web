import {observable, action, computed} from 'mobx';
import firebase from 'firebase/app';

import cloudFunctions from 'firebase/cloudFunctions';
import {stores} from 'stores';
import moment from 'moment';

export default class ProjectModel {
  @observable projectId;
  @observable projectName;
  @observable _team;
  @observable color;
  @observable _sections;

  constructor(data) {
    this.projectId = data.projectId;
    this.projectName = data.projectName;
    this.color = data.color;
    this._team = Object.values(data.team);
  }

  @computed
  get tasks() {
    return [...stores.taskStore.tasks.values()].filter(t => t.projectId === this.projectId);
  }

  @computed
  get team() {
    return this._team.map(member => {
      const user = stores.userStore.users.get(member.userId);

      if (!user) return member;

      user.role = member.role;
      user.userId = member.userId;
      return user;
    });
  }

  @action
  delete() {
    return cloudFunctions.deleteProject({projectId: this.projectId});
  }

  @action
  leave() {
    return cloudFunctions.leaveProject({projectId: this.projectId});
  }

  @action
  changeColor(color) {
    if (!color) return;

    return firebase
      .firestore()
      .collection('projects')
      .doc(this.projectId)
      .update({color});
  }

  @action
  changeName(projectName) {
    if (!projectName) return;

    return firebase
      .firestore()
      .collection('projects')
      .doc(this.projectId)
      .update({projectName});
  }

  @action
  kickUser(userId) {
    return cloudFunctions.kickFromProject({projectId: this.projectId, userId});
  }

  @action
  changeMemberRole(userId, newRole) {
    return cloudFunctions.changeRoleOfMember({projectId: this.projectId, userId, newRole});
  }

  @computed
  get selfRole() {
    const member = this._team.find(member => member.userId === stores.authStore.user.uid);
    return member?.role;
  }

  async createTask(text) {
    const res = await firebase
      .firestore()
      .collection('tasks')
      .add({
        projectId: this.projectId,
        sectionId: null,
        text: 'MÜKEMMEL TASK',
        assigned: {},
        status: 0,
        createdBy: stores.authStore.user.uid,
        creationDate: new Date().getTime(),
        completedBy: null,
        completionDate: null,
      });
  }
}
