import {observable, action, computed} from 'mobx';
import firebase from 'firebase/app';

import cloudFunctions from 'firebase/cloudFunctions';
import {stores} from 'stores';

export default class TaskModel {
  @observable projectId;
  @observable taskId;
  @observable sectionId;
  @observable text;
  @observable assigned;
  @observable status;
  @observable createdBy;
  @observable creationDate;
  @observable completedBy;
  @observable completionDate;

  constructor(data) {
    this.projectId = data.projectId;
    this.taskId = data.taskId;
    this.sectionId = data.sectionId;
    this.text = data.text;
    this.assigned = Object.keys(data.assigned);
    this.status = data.status;
    this.createdBy = data.createdBy;
    this.creationDate = data.creationDate;
    this.completedBy = data.completedBy;
    this.completionDate = data.completionDate;
  }

  @computed
  get assignedUsers() {
    return this.assigned.map(userId => stores.userStore.users.get(userId));
  }

  @computed
  get project() {
    return stores.projectStore._projects.get(this.projectId);
  }

  @computed
  get creator() {
    return stores.userStore.users.get(this.createdBy);
  }

  get firebaseDoc() {
    return firebase
      .firestore()
      .collection('tasks')
      .doc(this.taskId);
  }

  delete() {
    return this.firebaseDoc.delete();
  }

  complete() {
    return this.firebaseDoc.update({
      completedBy: stores.authStore.user.uid,
      completionDate: new Date().getTime(),
      status: 1,
    });
  }

  uncomplete() {
    return this.firebaseDoc.update({
      completedBy: null,
      completionDate: null,
      status: 0,
    });
  }

  assignTo(userId) {
    return this.firebaseDoc.update({[`assigned.${userId}`]: true});
  }

  unassignFrom(userId) {
    return this.firebaseDoc.update({
      [`assigned.${userId}`]: firebase.firestore.FieldValue.delete(),
    });
  }
}
