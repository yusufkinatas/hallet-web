import styled from 'styled-components';
import {Layout} from 'antd';

export const AppWrapper = styled(Layout)`
  min-height: 100vh;
  display: flex;
  flex-direction: column;

  h1 {
    font-size: 2em;
  }
`;

export const ScreenWrapper = styled(Layout)`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  width: 100%;
  min-height: 100%;
  background: ${p => p.theme.greyLightest};
`;

export const Version = styled.div`
  position: fixed;
  bottom: 0;
  right: 0;
  color: white;
  background: #0009;
  padding: 3px;
  font-size: 10px;
`;
