import styled from 'styled-components';
import {Layout} from 'antd';

export const Wrapper = styled(Layout.Sider)`
  color: ${p => p.theme.greyLightest};
  display: none;
`;
