import React, {Component} from 'react';
import {Wrapper} from './HeaderStyled';
import HeaderMenu from './HeaderMenu';
import {inject, observer} from 'mobx-react';

@inject('authStore')
@observer
class Header extends Component {
  render() {
    const {authStore} = this.props;
    const {authenticated} = authStore;
    return (
      <Wrapper>
        <span>Hallet</span>
        {authenticated && <HeaderMenu />}
      </Wrapper>
    );
  }
}

export default Header;
