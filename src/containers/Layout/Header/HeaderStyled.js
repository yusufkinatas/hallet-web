import {Layout} from 'antd';
import styled from 'styled-components';

export const Wrapper = styled(Layout.Header)`
  font-size: 2em;
  color: ${p => p.theme.darker} !important;
  background: ${p => p.theme.white};
  box-shadow: 0 1px 3px 0 rgba(21, 27, 38, 0.15);
  z-index: 1;
  position: fixed;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
