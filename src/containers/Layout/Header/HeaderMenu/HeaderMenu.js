import React, {Component} from 'react';
import UserAvatar from 'components/UserAvatar';
import {inject, observer} from 'mobx-react';
import {Dropdown, Menu} from 'antd';
import ProfileSettingsModal from 'components/ProfileSettingsModal';

@inject('authStore')
@observer
class HeaderMenu extends Component {
  handleLogout = () => {
    const {authStore} = this.props;
    authStore.signOut();
  };

  showProfileSettings = () => {
    this.profileSettingsModal.show();
  };

  render() {
    const {authStore} = this.props;
    const {user} = authStore;

    return (
      <React.Fragment>
        <Dropdown
          placement="bottomLeft"
          trigger="click"
          overlay={
            <Menu>
              <Menu.Item onClick={this.showProfileSettings}>Profile Settings</Menu.Item>
              <Menu.Item onClick={this.handleLogout}>Logout</Menu.Item>
            </Menu>
          }>
          <div style={{display: 'flex', cursor: 'pointer'}}>
            <UserAvatar size="large" src={user.avatar} name={user.shortname} />
          </div>
        </Dropdown>
        <ProfileSettingsModal ref={r => (this.profileSettingsModal = r)} />
      </React.Fragment>
    );
  }
}

export default HeaderMenu;
