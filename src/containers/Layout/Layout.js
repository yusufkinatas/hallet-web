import React, {Component} from 'react';

import {Layout} from 'antd';

import Header from './Header';
import Sidebar from './Sidebar';
import {ScreenWrapper, AppWrapper, Version} from './LayoutStyled';
import packageJson from '../../../package.json';

class _Layout extends Component {
  render() {
    return (
      <AppWrapper>
        <Sidebar />
        <Layout>
          <Header />
          <ScreenWrapper style={{paddingTop: 40}}>{this.props.children}</ScreenWrapper>
          <Version>v{packageJson.version}</Version>
        </Layout>
      </AppWrapper>
    );
  }
}
export default _Layout;
