import i18n from 'locales';

const translate = (key, options) => i18n.t(key, options);

export default translate;
