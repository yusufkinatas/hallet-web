const theme = {
  primary:        '#14aaf5',
  primaryLight:   '#43bfff',
  secondary:      '#f04e23',
  danger:         '#ff4d4f',

  white:          '#ffffff',
  greyLightest:   '#f6f8f9',
  greyLighter:    '#d5dce1',
  greyLight:      '#b7bfc7',
  grey:           '#848f99',
  greyDark:       '#636f79',
  dark:           '#43484f',
  darker:         '#151c26',
};

const overridedAntdVariables = {
  'primary-color': theme.primary,
  'text-selection-bg': theme.secondary,
};

module.exports = {theme, overridedAntdVariables};
