import React, {Component} from 'react';
import {Popover} from 'antd';

const colors = ['#902c2c', '#bf5e29', '#e0ac56', '#2F9458', '#18517e', '#202662', '#431e6c'];

class ColorPicker extends Component {
  render() {
    const {selectedColor, selectColor, visible, onVisibleChange} = this.props;
    return (
      <Popover
        trigger="click"
        visible={visible}
        onVisibleChange={onVisibleChange}
        content={
          <div style={{display: 'flex'}}>
            {colors.map(color => (
              <div
                key={color}
                onClick={() => selectColor(color)}
                style={{
                  width: 25,
                  height: 25,
                  background: color,
                  border: selectedColor === color && '3px solid white',
                  cursor: 'pointer',
                }}
              />
            ))}
          </div>
        }>
        <div
          style={{background: selectedColor || colors[0], cursor: 'pointer', width: 25, height: 25}}
        />
      </Popover>
    );
  }
}

export default ColorPicker;
