import React from 'react';
import {Avatar} from 'antd';
import {theme} from 'theme';

const UserAvatar = ({name, src, size}) => {
  return (
    <Avatar style={{backgroundColor: theme.dark, fontSize: 16}} size={size} src={src}>
      {name}
    </Avatar>
  );
};

export default UserAvatar;
