import React, {Component} from 'react';
import {Modal, Button, Upload, message, Avatar, Input} from 'antd';
import {inject, observer} from 'mobx-react';
import {UserOutlined} from '@ant-design/icons';

@inject('authStore')
@observer
class ProfileSettingsModal extends Component {
  state = {
    visible: false,
    uploading: false,
    removingPhoto: false,
    changingPassword: false,
    changingUsername: false,
    username: null,
    oldPassword1: null,
    oldPassword2: null,
    newPassword: null,
  };

  show = () => {
    this.setState({
      visible: true,
      oldPassword1: null,
      oldPassword2: null,
      newPassword: null,
      username: this.props.authStore.user.username,
    });
  };

  hide = () => {
    this.setState({visible: false});
  };

  handleOnChangeImage = info => {
    if (info.file.status === 'uploading') {
      this.setState({uploading: true});
      return;
    }
    if (info.file.status === 'done') {
      // Get this url from response in real world.
      this.getBase64(info.file.originFileObj, imageURL => {
        this.setState({
          uploading: false,
        });
      });
    }
  };

  handleUploadPhoto = async ({onError, onSuccess, file}) => {
    try {
      await this.props.authStore.updateAvatar(file);
      onSuccess(null);
    } catch (e) {
      console.log('HAHA', e);
      onError(e);
    }
  };

  handleRemovePhoto = async () => {
    try {
      this.setState({removingPhoto: true});
      await this.props.authStore.removeAvatar();
      this.setState({removingPhoto: false});
    } catch (error) {
      this.setState({removingPhoto: false});
    }
  };

  beforeUpload = file => {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      message.error('You can only upload JPG/PNG file!');
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      message.error('Image must smaller than 2MB!');
    }
    return isJpgOrPng && isLt2M;
  };

  getBase64 = (img, callback) => {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  };

  handleChangeOldPassword1 = e => {
    this.setState({oldPassword1: e.target.value});
  };

  handleChangeOldPassword2 = e => {
    this.setState({oldPassword2: e.target.value});
  };

  handleChangeNewPassword = e => {
    this.setState({newPassword: e.target.value});
  };

  changePassword = async () => {
    const {oldPassword1, oldPassword2, newPassword} = this.state;

    if (!oldPassword1 || !oldPassword2 || !newPassword)
      return message.error('Please fill necessary fields');

    if (oldPassword1 !== oldPassword2) return message.error('Passwords should be the same');

    try {
      this.setState({changingPassword: true});
      await this.props.authStore.changeUserPassword(oldPassword1, newPassword);
      message.success('Password changed');
      this.setState({
        changingPassword: false,
        oldPassword1: null,
        oldPassword2: null,
        newPassword: null,
      });
    } catch (error) {
      message.error(error);
      this.setState({changingPassword: false});
    }
  };

  handleChangeUsername = e => {
    this.setState({username: e.target.value});
  };

  changeUsername = async () => {
    const {username} = this.state;
    if (!username) return message.error('Please enter a username');

    try {
      this.setState({changingUsername: true});
      await this.props.authStore.updateUsername(username);
      message.success('Name changed');
      this.setState({changingUsername: false});
    } catch (error) {
      message.error(error);
      this.setState({changingUsername: false});
    }
  };

  render() {
    const {authStore} = this.props;
    const {user} = authStore;
    const {avatar} = user;

    const {
      visible,
      uploading,
      removingPhoto,
      changingPassword,
      oldPassword1,
      oldPassword2,
      newPassword,
      username,
      changingUsername,
    } = this.state;

    return (
      <Modal
        onCancel={this.hide}
        visible={visible}
        footer={null}
        title="Profile Settings"
        bodyStyle={{display: 'flex', flexDirection: 'column'}}>
        <div>
          <h3>Profile Picture</h3>
          <div style={{display: 'flex', alignItems: 'center'}}>
            <Upload
              name="avatar"
              showUploadList={false}
              customRequest={this.handleUploadPhoto}
              beforeUpload={this.beforeUpload}
              onChange={this.handleOnChangeImage}>
              <div style={{cursor: 'pointer'}}>
                <Avatar src={avatar} size="large" icon={<UserOutlined />} />
                <Button loading={uploading} type="link">
                  Upload New
                </Button>
              </div>
            </Upload>
            {avatar && (
              <Button loading={removingPhoto} type="link" onClick={this.handleRemovePhoto}>
                Remove
              </Button>
            )}
          </div>
        </div>

        <div>
          <h3>Full Name</h3>
          <Input onChange={this.handleChangeUsername} placeholder="Full Name" value={username} />
          <Button loading={changingUsername} onClick={this.changeUsername}>
            Change
          </Button>
        </div>
        <div>
          <h3>Change Password</h3>
          <Input
            placeholder="Old Password"
            onChange={this.handleChangeOldPassword1}
            value={oldPassword1}
          />
          <Input
            placeholder="Old Password(Again)"
            onChange={this.handleChangeOldPassword2}
            value={oldPassword2}
          />
          <Input
            placeholder="New Password"
            onChange={this.handleChangeNewPassword}
            value={newPassword}
          />
          <Button loading={changingPassword} onClick={this.changePassword}>
            Change
          </Button>
        </div>
      </Modal>
    );
  }
}

export default ProfileSettingsModal;
