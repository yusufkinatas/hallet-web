import React, {Component} from 'react';
import {CheckOutlined, CloseOutlined} from '@ant-design/icons';
import {Button, Collapse, Row, Col} from 'antd';
import {observer, inject} from 'mobx-react';
import Project from './Project';
import translate from 'utils/translate';

@inject('authStore', 'projectStore')
@observer
class Dashboard extends Component {
  state = {
    createProjectLoading: false,
  };

  renderProjects = () => {
    const {projectStore} = this.props;
    const {projects} = projectStore;

    return (
      <div style={{marginTop: 20}}>
        <h2>{translate('Dashboard.Projects')}</h2>
        <Button onClick={this.handleOnCreateProject} loading={this.state.createProjectLoading}>
          {translate('Dashboard.CreateNewProject')}
        </Button>
        <div style={{display: 'flex', flexDirection: 'column'}}>
          {projects.map(project => (
            <Project key={project.projectId} project={project} />
          ))}
        </div>
      </div>
    );
  };

  renderInvitations = () => {
    const {authStore} = this.props;
    const {invitations} = authStore;
    return (
      <Collapse>
        <Collapse.Panel
          header={translate('Dashboard.Invitations', {count: invitations.length})}
          key="1">
          {invitations.map(invitation => {
            const {projectId, projectName, inviterName} = invitation;
            return (
              <Row key={projectId} type="flex" align="middle" style={{marginBottom: 15}}>
                <Col span={12}>
                  <span>
                    <b>
                      {translate('Dashboard.InvitesToProject', {
                        name: inviterName,
                        project: projectName,
                      })}
                    </b>
                  </span>
                </Col>
                <Col span={12}>
                  <Button.Group>
                    <Button
                      icon={<CloseOutlined />}
                      onClick={() => authStore.rejectInvitation(invitation)}
                    />
                    <Button
                      icon={<CheckOutlined />}
                      onClick={() => authStore.acceptInvitation(invitation)}
                    />
                  </Button.Group>
                </Col>
              </Row>
            );
          })}
        </Collapse.Panel>
      </Collapse>
    );
  };

  handleOnCreateProject = async () => {
    this.setState({
      createProjectLoading: true,
    });
    const {projectStore} = this.props;

    await projectStore.createProject('Yeni Proje');

    // TODO PopUp üzerinden input al, createProject sonucu success message döndür
    this.setState({
      createProjectLoading: false,
    });
  };

  render() {
    return (
      <div style={{padding: 50, display: 'flex', flexDirection: 'column'}}>
        {this.renderInvitations()}

        {this.renderProjects()}
      </div>
    );
  }
}

export default Dashboard;
