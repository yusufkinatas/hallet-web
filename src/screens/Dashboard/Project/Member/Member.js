import React, {Component} from 'react';
import {theme} from 'theme';
import {CloseOutlined} from '@ant-design/icons';
import {Button, Menu, Dropdown, Modal} from 'antd';
import translate from 'utils/translate';
import {observer, inject} from 'mobx-react';
import UserAvatar from 'components/UserAvatar';

@inject('authStore')
@observer
class Member extends Component {
  state = {
    searching: false,
  };

  handleKickUser = () => {
    const {project, member} = this.props;
    Modal.confirm({
      okType: 'danger',
      title: 'Üyeyi projeden atmak istediğine emin misin?',
      content: 'Bu işlem geri alınamaz',
      onOk: async () => {
        await project.kickUser(member.uid);
      },
    });
  };

  get kickButtonVisible() {
    const {
      project,
      member: {role},
    } = this.props;
    const {selfRole} = project;

    if (selfRole === 'member' || role === 'owner' || (selfRole === 'admin' && role === 'admin')) {
      return false;
    }

    return true;
  }

  renderRole = () => {
    const {member, project} = this.props;
    const {selfRole} = project;
    const {role, username} = member;

    const menu = (
      <Menu>
        <Menu.Item key="1" onClick={() => this.handleChangeRole('member')}>
          {translate('Member.member')}
        </Menu.Item>
        <Menu.Item key="2" onClick={() => this.handleChangeRole('admin')}>
          {translate('Member.admin')}
        </Menu.Item>
        <Menu.Item key="3" onClick={() => this.handleChangeRole('owner')}>
          {translate('Member.owner')}
        </Menu.Item>
      </Menu>
    );

    if (selfRole === 'owner') {
      if (role === 'owner') {
        return (
          <div>
            <b>{username} - </b>
            <span>{translate('Member.' + role.toString())}</span>
          </div>
        );
      }
      return (
        <div>
          <b>{username} - </b>
          <Dropdown trigger={['click']} overlay={menu}>
            <Button style={{paddingLeft: 0}} type="link" onClick={e => e.preventDefault()}>
              {translate('Member.' + role.toString())}
            </Button>
          </Dropdown>
        </div>
      );
    } else {
      return (
        <span>
          {username} - <b>{translate('Member.' + role.toString())}</b>
        </span>
      );
    }
  };

  handleChangeRole(newRole) {
    const {member, project} = this.props;

    project.changeMemberRole(member.uid, newRole);
  }

  render() {
    const {member, authStore} = this.props;
    const {uid, email, shortname, avatar} = member;

    return (
      <div
        key={uid}
        style={{
          display: 'flex',
          marginTop: 5,
          padding: 5,
          border: `${uid === authStore.user.uid ? '3px' : '1px'} solid #ccc`,
          alignItems: 'center',
        }}>
        <UserAvatar name={shortname} src={avatar} />
        <div style={{display: 'flex', flexDirection: 'column', flex: 1}}>
          {this.renderRole()}
          {email}
        </div>
        {this.kickButtonVisible && (
          <Button
            icon={<CloseOutlined />}
            style={{color: theme.danger, cursor: 'pointer', border: 'none'}}
            onClick={this.handleKickUser}
          />
        )}
      </div>
    );
  }
}

export default Member;
