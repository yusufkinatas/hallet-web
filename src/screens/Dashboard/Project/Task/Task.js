import React, {Component} from 'react';
import UserAvatar from 'components/UserAvatar';
import {Button, Menu, Dropdown} from 'antd';

import {DeleteOutlined, UserAddOutlined, CheckOutlined} from '@ant-design/icons';

class Task extends Component {
  state = {deleteLoading: false, jej: false};

  handleDelete = async () => {
    try {
      this.setState({deleteLoading: true});

      await this.props.task.delete();
    } catch (error) {
      this.setState({deleteLoading: false});
    }
  };

  complete = () => {
    this.props.task.complete();
  };

  uncomplete = () => {
    this.props.task.uncomplete();
  };

  render() {
    const {task} = this.props;
    const {deleteLoading} = this.state;
    return (
      <div style={{border: 'solid 1px #ccc', padding: 8, marginTop: 8}}>
        <div>
          <Button
            onClick={task.status === 0 ? this.complete : this.uncomplete}
            icon={<CheckOutlined />}
            type={task.status === 0 ? 'link' : 'primary'}
          />
          <span>{task.text}</span>
          <Button
            loading={deleteLoading}
            danger
            type="link"
            icon={<DeleteOutlined />}
            onClick={this.handleDelete}
          />
          <Dropdown
            trigger="click"
            overlay={
              <Menu onClick={item => task.assignTo(item.key)}>
                {task.project.team
                  .filter(member => !task.assignedUsers.find(user => user?.uid === member.userId))
                  .map(member => (
                    <Menu.Item id={member.userId} key={member.userId}>
                      <UserAvatar size="small" name={member?.shortname} src={member?.avatar} />
                      {member.username}
                    </Menu.Item>
                  ))}
              </Menu>
            }>
            <Button link icon={<UserAddOutlined />} />
          </Dropdown>
        </div>
        <div style={{display: 'flex'}}>
          {task.assignedUsers.map(user => (
            <div style={{cursor: 'pointer'}} onClick={() => task.unassignFrom(user.userId)}>
              <UserAvatar src={user?.avatar} name={user?.shortname} />
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default Task;
