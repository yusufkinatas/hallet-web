import React, {Component} from 'react';
import {DeleteOutlined, LogoutOutlined} from '@ant-design/icons';
import {Card, Button, Popover, Input, List, Modal, Tabs} from 'antd';
import firebase from 'firebase/app';
import {observer, inject} from 'mobx-react';
import ColorPicker from 'components/ColorPicker';
import Member from './Member';
import translate from 'utils/translate';
import UserAvatar from 'components/UserAvatar';
import Task from './Task';

const searchDelay = 250;

@inject('authStore', 'projectStore')
@observer
class Project extends Component {
  state = {
    filter: '',
    searchResults: [],
    searching: false,
    colorPickerVisible: false,
    isEditingName: false,
    inviteLoading: false,
  };

  handleOnChangeFilter = async e => {
    const filter = e.target.value;
    clearTimeout(this.timer);

    if (!filter.length) {
      return this.setState({searchResults: [], searching: false, filter});
    }

    this.setState({searching: true, filter});
    this.timer = setTimeout(async () => {
      var min = filter;
      var max =
        filter.slice(0, -1) +
        String.fromCharCode(parseInt(filter.charCodeAt(filter.length - 1) + 1));

      const users = new Map();

      const resUsername = await firebase
        .firestore()
        .collection('users')
        .orderBy('username')
        .startAt(min)
        .endAt(max)
        .limit(10)
        .get();

      const resEmail = await firebase
        .firestore()
        .collection('users')
        .orderBy('email')
        .startAt(min)
        .endAt(max)
        .limit(10)
        .get();

      [...resUsername.docs, ...resEmail.docs].forEach(
        snapshot =>
          !users.has(snapshot.id) && users.set(snapshot.id, {uid: snapshot.id, ...snapshot.data()})
      );

      this.setState({searching: false, searchResults: [...users.values()]});
    }, searchDelay);
  };

  handleInviteVisibleChange = visible => {
    if (visible) {
      this.setState({filter: '', searchResults: [], searching: false});
      setTimeout(() => {
        this.searchInput.focus();
      }, 0);
    }
  };

  handleOnSearchResultClick = async user => {
    const {uid} = user;
    const {
      project: {projectId},
    } = this.props;

    this.setState({
      inviteLoading: true,
    });

    await this.props.projectStore.inviteUserToProject(projectId, uid);

    this.setState({
      inviteLoading: false,
    });
  };

  handleColorPickerVisibleChange = visible => {
    this.setState({colorPickerVisible: visible});
  };

  handleSelectColor = color => {
    this.props.project.changeColor(color);
    this.setState({colorPickerVisible: false});
  };

  handleChangeName = () => {
    this.props.project.changeName(this.editedName);
    this.setState({isEditingName: false});
  };

  handleDelete = () => {
    Modal.confirm({
      okType: 'danger',
      title: 'Projeyi silmek istediğine emin misin?',
      content: 'Bu işlem geri alınamaz',
      onOk: async () => {
        await this.props.project.delete();
      },
    });
  };

  handleLeave = () => {
    Modal.confirm({
      okType: 'danger',
      title: 'Projeden ayrılmak istediğine emin misin?',
      content: 'Bu işlem geri alınamaz',
      onOk: async () => {
        await this.props.project.leave();
      },
    });
  };

  handleCreateSection = () => {};

  handleCreateTask = () => {
    this.props.project.createTask();
  };

  renderTasks = () => {
    const {
      project: {tasks},
    } = this.props;

    return (
      <Button.Group>
        <Button onClick={this.handleCreateSection}>Create Section</Button>
        <Button onClick={this.handleCreateTask}>Create Task</Button>
        {tasks.map(task => (
          <Task key={task.taskId} task={task} />
        ))}
      </Button.Group>
    );
  };

  render() {
    const {project} = this.props;
    const {projectName, projectId, team, color} = project;
    const {
      searching,
      searchResults,
      filter,
      colorPickerVisible,
      isEditingName,
      inviteLoading,
    } = this.state;
    return (
      <Card
        key={projectId}
        title={
          isEditingName ? (
            <Input.Group style={{width: '80%'}} compact>
              <Input
                onChange={val => (this.editedName = val.target.value)}
                autoFocus
                defaultValue={projectName}
              />
              <Button onClick={this.handleChangeName}>{translate('General.Save')}</Button>
            </Input.Group>
          ) : (
            <span onClick={() => this.setState({isEditingName: true})} style={{cursor: 'pointer'}}>
              {projectName}
            </span>
          )
        }
        style={{marginTop: 10}}
        bodyStyle={{paddingTop: 0, display: 'flex', flexDirection: 'column'}}>
        <Tabs style={{paddingTop: 0}} defaultActiveKey="1">
          <Tabs.TabPane tab="Tasks" key="1">
            {this.renderTasks()}
          </Tabs.TabPane>
          <Tabs.TabPane tab="Team" key="2">
            {team.map(member => (
              <Member
                project={project}
                member={member}
                key={member.userId}
                selfRole={project.selfRole}
              />
            ))}
            {(project.selfRole === 'owner' || project.selfRole === 'admin') && (
              <Popover
                trigger="click"
                onVisibleChange={this.handleInviteVisibleChange}
                content={
                  <div>
                    <List
                      style={{maxHeight: 300, overflow: 'scroll'}}
                      loading={searching || inviteLoading}
                      itemLayout="horizontal"
                      dataSource={searchResults}
                      renderItem={item => {
                        const alreadyInTeam =
                          team.findIndex(member => member.userId === item.uid) !== -1;
                        return (
                          <List.Item
                            style={{
                              cursor: !alreadyInTeam && 'pointer',
                              backgroundColor: alreadyInTeam && '#ddd',
                            }}
                            onClick={() => !alreadyInTeam && this.handleOnSearchResultClick(item)}>
                            <List.Item.Meta
                              disabled
                              title={item.username}
                              description={item.email}
                            />
                          </List.Item>
                        );
                      }}
                    />
                    <Input
                      value={filter}
                      ref={r => (this.searchInput = r)}
                      onChange={this.handleOnChangeFilter}
                    />
                  </div>
                }>
                <Button style={{marginTop: 10}}>{translate('General.Invite')}</Button>
              </Popover>
            )}
          </Tabs.TabPane>
          <Tabs.TabPane tab="Settings" key="3">
            <ColorPicker
              selectedColor={color}
              selectColor={this.handleSelectColor}
              visible={colorPickerVisible}
              onVisibleChange={this.handleColorPickerVisibleChange}
            />
            {project.selfRole === 'owner' ? (
              <Button icon={<DeleteOutlined />} danger onClick={this.handleDelete}>
                {translate('General.Delete')}
              </Button>
            ) : (
              <Button danger icon={<LogoutOutlined />} onClick={this.handleLeave}>
                Leave
              </Button>
            )}
          </Tabs.TabPane>
        </Tabs>
      </Card>
    );
  }
}

export default Project;
