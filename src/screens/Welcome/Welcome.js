import React, {Component} from 'react';
import {LoginOutlined} from '@ant-design/icons';
import {Button, Input} from 'antd';
import {observer, inject} from 'mobx-react';
import translate from 'utils/translate';

@inject('authStore')
@observer
class Welcome extends Component {
  state = {
    email: 'test@gmail.com',
    password: '00000000',
    username: 'test',
    isSignin: true,
    loading: false,
  };

  onChangeUsername = e => {
    const username = e.target.value;
    this.setState({username});
  };

  onChangeEmail = e => {
    const email = e.target.value;
    this.setState({email});
  };

  onChangePassword = e => {
    const password = e.target.value;
    this.setState({password});
  };

  toggleFormState = () => {
    this.setState({isSignin: !this.state.isSignin});
  };

  handleSubmit = async () => {
    const {email, password, username, isSignin} = this.state;
    const {authStore} = this.props;
    try {
      this.setState({loading: true});

      if (isSignin) {
        await authStore.signInWithEmailAndPassword(email, password);
      } else {
        await authStore.createUserWithEmailAndPassword({email, password, username});
      }
    } catch (error) {
      this.setState({loading: false});
    }
  };

  render() {
    const {email, password, isSignin, username, loading} = this.state;
    return (
      <div style={{padding: 50, display: 'flex', flexDirection: 'column'}}>
        {!isSignin && (
          <Input
            placeholder={translate('Welcome.UsernamePlaceholder')}
            value={username}
            onChange={this.onChangeUsername}
          />
        )}
        <Input
          placeholder={translate('Welcome.EmailPlaceholder')}
          value={email}
          onChange={this.onChangeEmail}
        />
        <Input
          placeholder={translate('Welcome.PasswordPlaceholder')}
          value={password}
          onChange={this.onChangePassword}
        />
        <Button
          loading={loading}
          onClick={this.handleSubmit}
          type="primary"
          icon={<LoginOutlined />}>
          {isSignin ? translate('Welcome.SignIn') : translate('Welcome.SignUp')}
        </Button>
        <Button onClick={this.toggleFormState} type="link">
          {isSignin ? translate('Welcome.CreateAccount') : translate('Welcome.AlreadyHaveAccount')}
        </Button>
      </div>
    );
  }
}

export default Welcome;
