import 'firebase/initialize';
import React from 'react';

import {Provider} from 'mobx-react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Routes from './routes';
import {stores} from 'stores';
import {ThemeProvider} from 'styled-components';

const {theme} = require('./theme');

function App() {
  return (
    <Provider {...stores}>
      <ThemeProvider theme={theme}>
        <Router>
          <Switch>
            <Route path="/" component={Routes} />
          </Switch>
        </Router>
      </ThemeProvider>
    </Provider>
  );
}

export default App;
