import {observable, action} from 'mobx';
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/database';
import 'firebase/auth';

import TaskModel from 'models/taskModel';

export default class TaskStore {
  @observable tasks = new Map();

  dbTaskListenerForProject = {};

  @action
  addListenerForTasks(projectId) {
    if (this.dbTaskListenerForProject[projectId]) return;

    this.dbTaskListenerForProject[projectId] = firebase
      .firestore()
      .collection('tasks')
      .where('projectId', '==', projectId)
      .onSnapshot(snapshot => {
        snapshot.docChanges().forEach(change => {
          const task = change.doc.data();
          if (change.type === 'added') {
            this.handleTask(change.doc.id, task);
          }
          if (change.type === 'modified') {
            this.handleTask(change.doc.id, task);
          }
          if (change.type === 'removed') {
            this.deleteTask(change.doc.id);
          }
        });
      });
  }

  @action
  handleTask = (taskId, task) => {
    this.tasks.set(taskId, new TaskModel({...task, taskId}));
  };

  @action
  deleteTask = taskId => {
    this.tasks.delete(taskId);
  };

  @action
  clearStore() {
    this.tasks = new Map();
    Object.values(this.dbTaskListenerForProject).forEach(listener => listener());
    this.dbTaskListenerForProject = {};
  }
}
