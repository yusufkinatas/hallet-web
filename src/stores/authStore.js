import {observable, action} from 'mobx';
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/database';
import 'firebase/auth';
import UserModel from 'models/userModel';
import {stores} from './index.js';
import cloudFunctions from 'firebase/cloudFunctions';
import {message} from 'antd';

export default class AuthStore {
  @observable authenticated = null;
  @observable user = new UserModel({});
  @observable invitations = [];
  @observable dbListenerForUser;

  constructor() {
    this.handleAuth();
  }

  createUserWithEmailAndPassword = async ({email, password, username}) => {
    try {
      await firebase.auth().createUserWithEmailAndPassword(email, password);
      await cloudFunctions.initializeUser({username});
    } catch (error) {
      message.error(error.message);
      throw error.message;
    }
  };

  @action
  listenFirestore() {
    this.dbListenerForUser = firebase
      .firestore()
      .collection('users')
      .doc(this.user.uid)
      .onSnapshot(snapshot => {
        const userData = snapshot.data();
        if (!userData) return;

        const {projects, invitations, username, avatar} = userData;

        this.user.username = username;
        this.user.avatar = avatar;
        this.invitations = Object.values(invitations);
        stores.projectStore.handleProjectIds(Object.keys(projects));
      });
  }

  @action
  handleAuth = () => {
    firebase.auth().onAuthStateChanged(async user => {
      if (user) {
        this.authenticated = true;
        this.user = new UserModel({
          uid: user.uid,
          email: user.email,
          username: user.displayName || user.uid,
          avatar: user.photoURL,
        });
        this.listenFirestore();
      } else {
        this.authenticated = false;
        this.authenticated = false;
        this.invitations = [];
        this.user = new UserModel({});
        if (this.dbListenerForUser) {
          this.dbListenerForUser();
          this.dbListenerForUser = null;
        }
        stores.projectStore.clearStore();
        stores.userStore.clearStore();
      }
    });
  };

  signInWithEmailAndPassword = async (email, password) => {
    try {
      await firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION);
      await firebase.auth().signInWithEmailAndPassword(email, password);
    } catch (error) {
      message.error(error.message);
      throw error.message;
    }
  };

  signOut = () => {
    return firebase.auth().signOut();
  };

  rejectInvitation = invitation => {
    return cloudFunctions.respondToInvitation({invitation, accepted: false});
  };

  acceptInvitation = invitation => {
    return cloudFunctions.respondToInvitation({invitation, accepted: true});
  };

  updateAvatar = async file => {
    try {
      const {uid} = this.user;
      const fileRef = firebase.storage().ref(`users/${uid}/avatar.png`);
      await fileRef.put(file, {contentType: 'image/jpeg'});

      const photoURL = await fileRef.getDownloadURL();
      await firebase.auth().currentUser.updateProfile({photoURL});
      await firebase
        .firestore()
        .collection('users')
        .doc(uid)
        .update({avatar: photoURL});
    } catch (error) {
      throw error.message;
    }
  };

  removeAvatar = async () => {
    const {uid} = this.user;

    await firebase.auth().currentUser.updateProfile({photoURL: null});
    await firebase
      .firestore()
      .collection('users')
      .doc(uid)
      .update({avatar: ''});

    const fileRef = firebase.storage().ref(`users/${uid}/avatar.png`);
    await fileRef.delete();
  };

  reauthenticate = currentPassword => {
    const user = firebase.auth().currentUser;
    const cred = firebase.auth.EmailAuthProvider.credential(user.email, currentPassword);
    return user.reauthenticateWithCredential(cred);
  };

  changeUserPassword = async (currentPassword, newPassword) => {
    try {
      await this.reauthenticate(currentPassword);
      await firebase.auth().currentUser.updatePassword(newPassword);
    } catch (error) {
      throw error.message;
    }
  };

  updateUsername = async username => {
    const {uid} = this.user;
    username = username.trim();

    await firebase.auth().currentUser.updateProfile({displayName: username});
    await firebase
      .firestore()
      .collection('users')
      .doc(uid)
      .update({username});
  };
}
