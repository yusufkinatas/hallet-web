import {observable, action} from 'mobx';
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/database';
import 'firebase/auth';

import UserModel from 'models/userModel';

export default class UserStore {
  @observable users = new Map();
  dbListenersForUsers = {};

  @action
  addListenerForUser(userId) {
    if (this.dbListenersForUsers[userId]) return;

    this.dbListenersForUsers[userId] = firebase
      .firestore()
      .collection('users')
      .doc(userId)
      .onSnapshot(snapshot => {
        const data = snapshot.data();
        const {email, username, avatar} = data;
        const necessaryData = {email, username, avatar, uid: snapshot.id};
        this.users.set(snapshot.id, new UserModel(necessaryData));
      });
  }

  @action
  clearStore() {
    this.users = new Map();
    Object.values(this.dbListenersForUsers).forEach(listener => listener());
    this.dbListenersForUsers = {};
  }
}
