import {observable, action, computed} from 'mobx';
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/database';
import 'firebase/auth';

import cloudFunctions from 'firebase/cloudFunctions';
import ProjectModel from 'models/projectModel';
import {stores} from 'stores';

export default class ProjectStore {
  @observable _projects = new Map();
  dbListenersForProjects = {};

  @computed
  get projects() {
    return [...this._projects.values()];
  }

  @action
  addListenerForProject(projectId) {
    this.dbListenersForProjects[projectId] = firebase
      .firestore()
      .collection('projects')
      .doc(projectId)
      .onSnapshot(snapshot => {
        const data = snapshot.data();
        this._projects.set(snapshot.id, new ProjectModel(data));
        Object.keys(data.team).forEach(userId => stores.userStore.addListenerForUser(userId));
      });
  }

  @action
  handleProjectIds(projectIds) {
    const deletedProjectIds = [...this._projects.keys()].filter(
      projectId => !projectIds.includes(projectId)
    );

    deletedProjectIds.forEach(projectId => {
      this.handleDelete(projectId);
    });

    projectIds.forEach(projectId => {
      if (this.dbListenersForProjects[projectId]) return;
      this.addListenerForProject(projectId);
      stores.taskStore.addListenerForTasks(projectId);
    });
  }

  @action
  handleDelete(projectId) {
    this._projects.delete(projectId);
    if (this.dbListenersForProjects[projectId]) {
      this.dbListenersForProjects[projectId]();
      delete this.dbListenersForProjects[projectId];
    }
  }

  @action
  clearStore() {
    this._projects = new Map();
    Object.values(this.dbListenersForProjects).forEach(listener => listener());
    this.dbListenersForProjects = {};
  }

  createProject(projectName) {
    return cloudFunctions.createProject({projectName});
  }

  inviteUserToProject(projectId, invitedId) {
    return cloudFunctions.inviteToProject({projectId, invitedId});
  }
}
