import AuthStore from './authStore';
import ProjectStore from './projectStore';
import UserStore from './userStore';
import TaskStore from './taskStore';

class Stores {
  constructor() {
    this.authStore = new AuthStore(this);
    this.projectStore = new ProjectStore(this);
    this.userStore = new UserStore(this);
    this.taskStore = new TaskStore(this);
  }
}

export const stores = new Stores();
