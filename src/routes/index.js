import React, {Component} from 'react';
import {inject, observer} from 'mobx-react';
import {Route, Switch, Redirect} from 'react-router-dom';
import Welcome from 'screens/Welcome';
import Dashboard from 'screens/Dashboard';
import Layout from 'containers/Layout';

// const RestrictedRoute = ({component: Component, authenticated, ...rest}) => (
//   <Route
//     {...rest}
//     render={props =>
//       authenticated ? (
//         <Component {...props} />
//       ) : (
//         <Redirect
//           to={{
//             pathname: '/signin',
//             state: {from: props.location},
//           }}
//         />
//       )
//     }
//   />
// );

@inject('authStore')
@observer
class Routes extends Component {
  // componentDidMount() {
  //   const {authenticated, handleAuth} = this.props.AuthStore;
  //   console.log(authenticated, this.props.location.pathname);
  //   if (authenticated === null)
  //     handleAuth();
  //   else {
  //     console.log('location', this.props.location.pathname);
  //     localStorage.setItem('location', this.props.location.pathname);
  //   }
  // }

  render() {
    const {location} = this.props;
    const {authenticated} = this.props.authStore;

    if (authenticated === null) {
      return <div>Loading</div>;
    }

    if (location.pathname === '/' && authenticated) {
      return <Redirect to={'/dashboard'} />;
    } else if (location.pathname !== '/' && !authenticated) {
      return <Redirect to={'/'} />;
    }

    return (
      <Layout>
        <Switch>
          <Route exact path="/" component={Welcome} />
          <Route exact path="/dashboard" component={Dashboard} />
        </Switch>
      </Layout>
    );
  }
}

export default Routes;
