import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import 'firebase/functions';
import 'firebase/storage';
import {config} from './config';

firebase.initializeApp(config);
const functions = firebase.functions();
console.log(`NODE_ENV = ${process.env.NODE_ENV}`);
if (process.env.NODE_ENV === 'development') {
  functions.useFunctionsEmulator('http://localhost:5001');
}
