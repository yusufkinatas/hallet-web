import firebase from 'firebase/app';
import 'firebase/functions';
import {message} from 'antd';
import translate from 'utils/translate';

const generator = functionName => {
  return async params => {
    try {
      const res = await firebase
        .app()
        .functions('europe-west1')
        .httpsCallable(functionName)(params);
      const {success, message: responseMessage} = res.data;

      if (success) {
        message.success(translate(responseMessage));
      } else {
        message.error(translate(responseMessage));
      }

      return res;
    } catch (error) {
      message.error(translate('General.UnexpectedError'));
    }
  };
};

const cloudFunctions = {
  initializeUser: generator('initializeUser'),
  createProject: generator('createProject'),
  deleteProject: generator('deleteProject'),
  inviteToProject: generator('inviteToProject'),
  respondToInvitation: generator('respondToInvitation'),
  kickFromProject: generator('kickFromProject'),
  leaveProject: generator('leaveProject'),
  changeRoleOfMember: generator('changeRoleOfMember'),
};

export default cloudFunctions;
