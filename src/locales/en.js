export const en = {
  Test: 'Test',

  Welcome: {
    UsernamePlaceholder: 'Username',
    EmailPlaceholder: 'E-Mail',
    PasswordPlaceholder: 'Password',
    SignIn: 'Sign In',
    SignUp: 'Sign Up',
    CreateAccount: 'Create Account',
    AlreadyHaveAccount: 'I have an account',
  },

  Auth: {
    UserInitialize: 'User Initialized',
  },

  Dashboard: {
    Projects: 'Projects',
    CreateNewProject: 'Create New Project',
    Invitations: 'Invitations ({{count}})',
    InvitesToProject: '"{{name}}" invites you to join "{{project}}"',
  },

  Project: {
    Members: 'Members',
    ProjectLimitReached: 'Maximum project limit(5) reached',
    ProjectCreateSuccess: 'Project Created',
    ProjectDeleteSucces: 'Project Deleted',
    UserAlreadyInvited: 'User already invited',
    UserAlreadyExist: 'User already exist in project',
    UserInviteSuccess: 'User Invited',
    InvitationInvalid: "This invitation doesn't exist",
    InvitationRejected: 'Invitation rejected',
    InvitedProjectNotExist: "This project doesn't exist",
    InvivationSuccess: 'Invitation successfully accepted',
    UserKickSuccess: 'User kicked',
  },

  Member: {
    owner: 'Owner',
    admin: 'Admin',
    member: 'Member',
    ChangeRole: 'Change Role',
  },

  General: {
    Save: 'Save',
    Invite: 'Invite',
    Delete: 'Delete',
    PermissionDenied: 'Permission Denied',
    UnexpectedError: 'Unexpected Error!',
  },
};
