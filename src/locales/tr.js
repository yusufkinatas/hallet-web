export const tr = {
  Test: 'Test',

  Welcome: {
    UsernamePlaceholder: 'Kullanıcı Adı',
    EmailPlaceholder: 'E-Posta',
    PasswordPlaceholder: 'Şifre',
    SignIn: 'Giriş Yap',
    SignUp: 'Kayıt Ol',
    CreateAccount: 'Hesap Oluştur',
    AlreadyHaveAccount: 'Zaten hesabım var',
  },

  Auth: {
    UserInitialize: 'User Initialized',
  },

  Dashboard: {
    Projects: 'Projeler',
    CreateNewProject: 'Yeni Proje Oluştur',
    Invitations: 'Davetler ({{count}})',
    InvitesToProject: '"{{name}}" kullanıcısı seni "{{project}}" isimli projeye davet etti',
  },

  Project: {
    Members: 'Üyeler',
    ProjectLimitReached: 'Maksimum proje limitine(5) ulaşıldı',
    ProjectCreateSuccess: 'Proje oluşturuldu',
    ProjectDeleteSucces: 'Proje silindi',
    UserAlreadyInvited: 'Üye zaten davet edildi',
    UserAlreadyExist: 'Üye zaten projede yer alıyor',
    UserInviteSuccess: 'Üye davet edildi',
    InvitationInvalid: 'Davet mevcut değil',
    InvitationRejected: 'Davet reddedildi',
    InvitedProjectNotExist: 'Proje mevcut değil',
    InvivationSuccess: 'Davet başarıyla kabul edildi',
    UserKickSuccess: 'Üye atıldı',
  },

  Member: {
    owner: 'Kurucu',
    admin: 'Yönetici',
    member: 'Üye',
    ChangeRole: 'Rolü Değiştir',
  },

  General: {
    Save: 'Kaydet',
    Invite: 'Davet Et',
    Delete: 'Sil',
    PermissionDenied: 'Bunu yapmaya yetkileriniz yok',
    UnexpectedError: 'Beklenmedik Hata!',
  },
};
