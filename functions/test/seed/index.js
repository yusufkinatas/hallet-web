const admin = require('firebase-admin');

const users = [
  {
    uid: 'uid1',
    username: 'User 1',
    email: 'user1@test.com',
  },
  {
    uid: 'uid2',
    username: 'User 2',
    email: 'user2@test.com',
  },
];

const projects = [];

const populateUsers = done => {
  Promise.all(users.map(user => admin.auth().createUser({uid: user.uid, email: user.email})))
    .then(() => done())
    .catch(() => {});
};

const populateProjects = done => {
  done();
};

const removeUsers = done => {
  Promise.all(users.map(user => admin.auth().deleteUser(user.uid)))
    .then(() => done())
    .catch(() => {});
};

module.exports = {users, projects, populateProjects, populateUsers, removeUsers};
