const admin = require('firebase-admin');
const {assert} = require('chai');
const {populateProjects, populateUsers, removeUsers, users, projects} = require('./seed');

const test = require('firebase-functions-test')(
  {
    databaseURL: 'https://hallet-test.firebaseio.com',
    projectId: 'hallet-test',
    storageBucket: 'hallet-test.appspot.com',
  },
  './firebase-config-test.json'
);

let myFunctions;

before(async () => {
  myFunctions = require('../index');
  const testConfig = require('../firebase-config-test.json');

  //to configure firebase-admin for "hallet-test" instead of "hallet"
  await admin.app().delete();
  admin.initializeApp({credential: admin.credential.cert(testConfig)});
});

after(async () => {
  test.cleanup();
});

beforeEach(populateUsers);
beforeEach(populateProjects);
afterEach(removeUsers);

describe('initializeUser', () => {
  const newUser = {uid: 'uid3', email: 'newUser@test.com'};

  beforeEach(async () => {
    await admin.auth().createUser(newUser);
  });

  afterEach(async () => {
    await admin.auth().deleteUser(newUser.uid);
    await admin
      .firestore()
      .collection('users')
      .doc(newUser.uid)
      .delete();
  });

  it('should create doc', async () => {
    const wrapped = test.wrap(myFunctions.initializeUser);

    const data = {username: 'New Username'};

    const {success} = await wrapped(data, {
      auth: {uid: newUser.uid, token: {email: newUser.email}},
    });

    assert.isTrue(success);

    const res = await admin
      .firestore()
      .collection('users')
      .doc(newUser.uid)
      .get();

    assert.equal(data.username, res.data().username);
    assert.equal(newUser.email, res.data().email);
  });

  it('should not create doc for non-existing user', async () => {
    const wrapped = test.wrap(myFunctions.initializeUser);

    const data = {username: 'New Username'};

    const {success} = await wrapped(data, {
      auth: {uid: 'randomUid', token: {email: 'randomEmail'}},
    });

    assert.isFalse(success);

    const res = await admin
      .firestore()
      .collection('users')
      .doc(newUser.uid)
      .get();

    assert.isNotObject(res.data());
  });
});
