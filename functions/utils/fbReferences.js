const admin = require('firebase-admin');

const getReference = collectionName => {
  return id => {
    const reference = admin
      .firestore()
      .collection(collectionName)
      .doc(id);

    reference.getData = async () => (await reference.get()).data();

    return reference;
  };
};

const userRef = getReference('users');
const projectRef = getReference('projects');

module.exports = {userRef, projectRef};
