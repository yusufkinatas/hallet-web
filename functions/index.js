const functions = require('firebase-functions');
const admin = require('firebase-admin');
const _ = require('lodash');
const autoId = require('./utils/autoId');
const {userRef, projectRef} = require('./utils/fbReferences');

const firebaseConfig = require('./firebase-config.json');

const deletedField = admin.firestore.FieldValue.delete();

const unexpectedError = {success: false, message: 'General.UnexpectedError'};

admin.initializeApp({
  credential: admin.credential.cert(firebaseConfig),
});

const createCallableFunction = func => {
  return functions.region('europe-west1').https.onCall(func);
};

exports.initializeUser = createCallableFunction(async (data, context) => {
  try {
    const {auth} = context;
    const {uid} = auth;

    const extraUserData = _.pick(data, ['username']);
    await admin.auth().updateUser(uid, {displayName: extraUserData.username});
    const user = await admin.auth().getUser(uid);

    await userRef(uid).set({
      projects: {},
      invitations: {},
      email: auth.token.email,
      avatar: user.photoURL || null,
      ...extraUserData,
    });

    return {success: true, message: 'Auth.UserInitialize'};
  } catch (error) {
    return unexpectedError;
  }
});

exports.createProject = createCallableFunction(async (data, context) => {
  const {projectName} = data;
  const {auth} = context;
  const {uid} = auth;

  //kullanıcı max 5 projede yer alabilir
  const userData = await userRef(uid).getData();

  // if (Object.keys(userData.projects).length > 4) {
  //   return {success: false, message: 'Project.ProjectLimitReached'};
  // }

  const projectId = autoId();

  await projectRef(projectId).set({
    projectName,
    projectId,
    color: '#18517e',
    team: {[uid]: {userId: uid, role: 'owner'}},
    sections: {},
    tasks: {},
  });

  await userRef(uid).update({
    [`projects.${projectId}`]: true,
  });

  return {success: true, message: 'Project.ProjectCreateSuccess'};
});

exports.deleteProject = createCallableFunction(async (data, context) => {
  const {projectId} = data;

  const {auth} = context;
  const {uid} = auth;

  const {team} = await projectRef(projectId).getData();

  const foundMember = team[uid];

  if (!foundMember || foundMember.role !== 'owner') {
    return {success: false, message: 'General.PermissionDenied'};
  }

  const promises = Object.keys(team).map(userId =>
    // create new async function and call it to return as promise
    (async () => {
      await userRef(userId).update({[`projects.${projectId}`]: deletedField});
    })()
  );
  await Promise.all(promises);

  await projectRef(projectId).delete();

  return {success: true, message: 'Project.ProjectDeleteSucces'};
});

exports.inviteToProject = createCallableFunction(async (data, context) => {
  const {projectId, invitedId} = data;
  const {auth} = context;
  const {uid: inviterId} = auth;
  const {displayName: inviterName} = await admin.auth().getUser(inviterId);

  const projectData = await projectRef(projectId).getData();

  const {team, projectName} = projectData;

  const userData = await userRef(invitedId).getData();

  if (userData.invitations[projectId]) {
    return {success: false, message: 'Project.UserAlreadyInvited'};
  }
  if (team[inviterId].role === 'member') {
    return {success: false, message: 'General.PermissionDenied'};
  }
  if (team[invitedId]) {
    return {success: false, message: 'Project.UserAlreadyExist'};
  }

  await userRef(invitedId).update({
    [`invitations.${projectId}`]: {
      projectId,
      projectName,
      inviterName,
    },
  });

  return {success: true, message: 'Project.UserInviteSuccess'};
});

exports.respondToInvitation = createCallableFunction(async (data, context) => {
  const {invitation, accepted} = data;
  const {auth} = context;
  const {uid} = auth;

  const {projectId} = invitation;

  const userData = await userRef(uid).getData();
  const {invitations} = userData;

  //eğer user içinde invitation yoksa hata ver
  if (!invitations[projectId]) {
    return {success: false, message: 'Project.InvitationInvalid'};
  }

  //davetiyeyi kullanıcıdan sil
  await userRef(uid).update({[`invitations.${projectId}`]: deletedField});

  const projectData = await projectRef(projectId).getData();

  if (!accepted) {
    return {success: true, message: 'Project.InvitationRejected'};
  }

  //böyle bir proje yoksa hata ver
  if (!projectData) {
    return {success: false, message: 'Project.InvitedProjectNotExist'};
  }

  //projeye userı ekle
  await projectRef(projectId).update({[`team.${uid}`]: {role: 'member', userId: uid}});

  //usera projeyi ekle
  await userRef(uid).update({[`projects.${projectId}`]: true});
  return {success: true, message: 'Project.InvivationSuccess'};
});

exports.kickFromProject = createCallableFunction(async (data, context) => {
  const {projectId, userId: targetId} = data;

  const {auth} = context;
  const {uid: selfId} = auth;

  const projectData = await projectRef(projectId).getData();
  const {team} = projectData;

  const targetRole = team[targetId].role;
  const selfRole = team[selfId].role;

  if (targetRole === 'owner') {
    return {success: false, message: 'General.PermissionDenied'};
  }

  //if kicking an admin, check if user is owner
  if (targetRole === 'admin' && selfRole !== 'owner') {
    return {success: false, message: 'General.PermissionDenied'};
  }

  //if kicking an member, check if user is admin or owner
  if (selfRole === 'member') {
    return {success: false, message: 'General.PermissionDenied'};
  }

  //remove user from project & project from user
  await userRef(targetId).update({[`projects.${projectId}`]: deletedField});

  await projectRef(projectId).update({[`team.${targetId}`]: deletedField});

  return {success: true, message: 'Project.UserKickSuccess'};
});

exports.leaveProject = createCallableFunction(async (data, context) => {
  const {projectId} = data;

  const {auth} = context;
  const {uid} = auth;

  const {team} = await projectRef(projectId).getData();

  const selfRole = team[uid].role;

  //if user is owner, return error
  if (selfRole === 'owner') {
    return {success: false, message: 'Owner cannot leave'};
  }

  //remove user from project & project from user
  await userRef(uid).update({[`projects.${projectId}`]: deletedField});

  await projectRef(projectId).update({[`team.${uid}`]: deletedField});

  return {success: true, message: 'Leaved project'};
});

exports.changeRoleOfMember = createCallableFunction(async (data, context) => {
  const {projectId, userId: targetId, newRole} = data;

  const {auth} = context;
  const {uid: selfId} = auth;

  const projectData = await projectRef(projectId).getData();
  const {team} = projectData;

  // only owner can change role
  const selfRole = team[selfId].role;
  if (selfRole !== 'owner') {
    return {success: false, message: 'General.PermissionDenied'};
  }

  // you cannot change your role
  if (selfId === targetId) {
    return {success: false, message: 'General.PermissionDenied'};
  }

  // new Role must be member or admin or owner //hackshield
  if (newRole !== 'member' && newRole !== 'admin' && newRole !== 'owner') {
    return {success: false, message: 'General.PermissionDenied'};
  }

  //update project users
  const updateObject = {
    [`team.${targetId}.role`]: newRole,
  };

  //there must be only 1 owner
  if (newRole === 'owner') {
    updateObject[`team.${selfId}.role`] = 'admin';
  }

  //set new role
  await projectRef(projectId).update(updateObject);

  return {success: true, message: 'Member.ChangeRole'};
});
