const {
  override,
  fixBabelImports,
  addLessLoader,
  addDecoratorsLegacy,
  useBabelRc,
} = require('customize-cra');
const path = require('path');
const rewireReactHotLoader = require('react-app-rewire-hot-loader');
const {overridedAntdVariables} = require('./src/theme');

const overrideProcessEnv = value => (config, env) => {
  config = rewireReactHotLoader(config, env);
  config.resolve.alias = {
    ...config.resolve.alias,
    'react-dom': '@hot-loader/react-dom',
  };
  config.resolve.modules = [path.join(__dirname, 'src')].concat(config.resolve.modules);
  return config;
};

module.exports = override(
  useBabelRc(),
  addDecoratorsLegacy(),
  overrideProcessEnv({
    VERSION: JSON.stringify(require('./package.json').version),
  }),
  fixBabelImports('import', {
    libraryName: 'antd',
    libraryDirectory: 'es',
    style: true,
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: {...overridedAntdVariables},
  })
);
