# Hallet #

## How to run project on local ##

### 1. Install Firebase CLI ###

https://firebase.google.com/docs/cli#install_the_firebase_cli

### 2. Install node_modules ###

```
yarn install
# OR
npm install
```

### 3. Install node_modules for Cloud Functions ###

```
cd ./functions
npm install 
(You can't use yarn install here)
```

### 4. Start React project & Cloud Functions emulator ###

```
npm run dev
# OR
yarn dev
```

### You are ready to go! ###

## How to run tests for cloud functions ##
```
cd ./functions

npm test
#OR to test when files change
npm run test:watch
```